import React,{Component} from 'react';
import HeaderBlock from '../../components/HeaderBlock';
import ContentBlock from '../../components/ContentBlock';
import Header from "../../components/Header";
import Paragraph from "../../components/Paragraph";
import CardList from "../../components/CardList"
import BackgroundBlock from "../../components/BackgroundBlock"
import FirebaseContext from "../../context/firebaseContext";
import { Button } from 'antd';
import { PoweroffOutlined } from '@ant-design/icons';
import {connect} from 'react-redux';
import * as actions from "../../actions";
import {bindActionCreators} from "redux";
class HomePage extends Component {
    state = {
        wordsArr:[],
    }

    componentDidMount() {
        const {getUserCardRef} = this.context;
        getUserCardRef().on('value',res=>{
            this.setState({
                wordsArr: res.val() || [] ,
            })
        })
    }



    handlerDeletedItem = (id) =>{
        const {wordsArr} = this.state;
        const {getUserCardRef} = this.context;
        const newWordArr = wordsArr.filter((word)=>word.id!=id);
        getUserCardRef().set(newWordArr);
    }

    handlerAddItem = ({eng,rus}) =>{
        const {wordsArr} = this.state;
        const {getUserCardRef} = this.context;
        getUserCardRef().set([...wordsArr,{
            id:+new Date(),
            eng,
            rus,
        }])

    }
    handlerLogout = () =>{
        const {logoutUser} = this.context;
        logoutUser();
    }

    render() {
        console.log('####: props',this.props)
        const {wordsArr} = this.state;
        const{
            countNumber,
            plusAction,
            minusAction
        } = this.props;
        return (
            <>
                <HeaderBlock>
                    <Button type="primary" icon={<PoweroffOutlined />} danger onClick={this.handlerLogout} danger>
                        Выйти
                    </Button>
                    <Header>
                        Наклейки по вселенной <br/> <br/> <span id="hp">Harry Potter!</span>
                    </Header>
                    <Paragraph>
                        Погрузись в мир великого волшебника!
                    </Paragraph>
                </HeaderBlock>
                <HeaderBlock>
                    {countNumber}
                    <Button onClick={()=>plusAction(1)} >
                        +1
                    </Button>
                    <Button onClick={()=>minusAction(1)} >
                        -1
                    </Button>
                </HeaderBlock>

                <BackgroundBlock >
                    <h2 style={{color:'white',width:'100%',zIndex:'2',fontFamily:'arial'}}>Проверь свои знания заклинаний:</h2>
                    <CardList  onAddItem={this.handlerAddItem} onDeletedItem={this.handlerDeletedItem} item={wordsArr}/>
                </BackgroundBlock>
                <div>

                </div>
                <BackgroundBlock src="img2">
                    <ContentBlock/>
                </BackgroundBlock>

            </>
        )
    }

}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => {
    return {
        countNumber:state.counter.count,
    };
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(actions,dispatch)

}

export default connect( mapStateToProps,mapDispatchToProps) (HomePage);
