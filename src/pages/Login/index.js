import React,{Component} from 'react';

import { Layout,Form, Input, Button } from 'antd';
import s from './Login.module.scss';
import 'antd/dist/antd.css';
import FirebaseContext from "../../context/firebaseContext";
import { Typography } from 'antd';
import RegistrationPage from "../Registration";
import ReactDOM from "react-dom";
import Firebase from "../../services/firebase";
import App from "../../App";


const { Title } = Typography;
const { Content } = Layout;

class LoginPage extends Component {

    state= {
        registration:false,
    }

    onFinish = ({email,password}) =>{
        const {signWithEmail,setUserUid} = this.context;
        const {history} = this.props;

        signWithEmail(email,password)
            .then(result=>{
                setUserUid(result.user.uid);
                localStorage.setItem('user',result.user.uid);
                history.push('/');
            })
    }
    onFinishFailed = (errorMsg) =>{
        console.log('####: errorMsg',errorMsg);
    }

    renderRegistration = () =>{
        this.setState({
            registration:true
        });
    }
    renderForm = () =>{

        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };

        return (
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Title level={4}>Войти</Title>
                <Form.Item
                    label="email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>


                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Войти
                    </Button>
                    <Button type="primary" style={{marginLeft:'10px'}} onClick={this.renderRegistration}>
                        Регистрация
                    </Button>
                </Form.Item>
            </Form>
        );
    }

    render() {
        const {registration} =this.state;
        console.log(this.props);
        return (
            <Layout>
                <Content>
                    <div className={s.root}>
                        <div className={s.wrap_form}>
                            {registration?<RegistrationPage/>:this.renderForm()}
                        </div>
                    </div>

                </Content>

            </Layout>
        );
    }

}

LoginPage.contextType = FirebaseContext;

export default LoginPage;
