import React,{Component} from 'react';

import { Layout,Form, Input, Button } from 'antd';
import s from './Registration.module.scss';
import 'antd/dist/antd.css';
import FirebaseContext from "../../context/firebaseContext";
import { Typography } from 'antd';

const { Title } = Typography;

const { Content } = Layout;

class RegistrationPage extends Component {
    onFinish = ({email,password}) =>{
        const {newUser} = this.context;
        newUser(email,password)
            .then(result=>{
                console.log('####: newUser',result)
            })
    }
    onFinishFailed = (errorMsg) =>{
        console.log('####: errorMsg',errorMsg);
    }
    renderForm = () =>{

        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };

        return (


            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Title style={{textCenter:'center'}} level={4}>Регистрация</Title>
                <Form.Item
                    label="email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>


                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Зарегистрироваться
                    </Button>
                </Form.Item>
            </Form>
        );
    }

    render() {

        return (
            this.renderForm()
        );
    }

}

RegistrationPage.contextType = FirebaseContext;

export default RegistrationPage;
