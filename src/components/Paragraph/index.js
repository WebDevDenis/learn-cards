import React from 'react';
import s from './Paragraph.module.scss';

const Paragraph = ({children}) => {
    return (
        <h1 className={s.paragraph}>{children}</h1>
        );
}

export default Paragraph;