import React from 'react';
import s from './Header.module.scss';

const Header = ({children}) => {
    return (
        <h1 style={s.h1}>{children}</h1>
        );
}

export default Header;