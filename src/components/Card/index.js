import React, { Component } from 'react';
import s from './Card.module.scss';
import cl from 'classnames';
import {CheckCircleOutlined,DeleteOutlined } from '@ant-design/icons';

class Card extends React.Component{

    state ={
        open:false,
        isRemember: false,
        accessOpen:true,
    }

    handlerCardClick = () => {
        if (!this.state.accessOpen){
            return false;
        }
        this.setState(({open}) =>{
            return{
                open:!open
            }
        });
    }
    handlerIsRememberClick = () => {
        this.setState( ({isRemember,open,accessOpen}) => {
            return {
                    isRemember: !isRemember,
                    open:!open,
                    accessOpen: !accessOpen,
                }
        });
    }
    handlerDeletedClick = () => {
       console.log('level 1')
        this.props.onDeleted();
    }

    render() {
        const {eng,rus} = this.props;
        const {open} = this.state;
        const {isRemember} = this.state

        return (
            <div className={s.dFlex}>
                <div
                    className={cl(
                        s.card,
                        {[s.open]:open},
                        {[s.isRemembered]:isRemember}
                        )}
                    onClick={this.handlerCardClick}
                >
                    <div className={s.cardInner}>
                        <div className={s.cardFront}>
                            { eng }
                        </div>
                        <div className={s.cardBack}>
                            { rus }
                        </div>
                    </div>

                </div>
                <div onClick={this.handlerIsRememberClick} className={s.iconBlock}>
                    <CheckCircleOutlined  />
                </div>
                <div onClick={this.handlerDeletedClick} className={s.iconBlock}>
                    <DeleteOutlined />
                </div>
        </div>
        );
    }
}


export default Card;