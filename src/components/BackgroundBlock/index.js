import React from 'react';
import s from './BackgroundBlock.module.scss';

const BackgroundBlock = ({src=false,children}) => {

    let back = <div className={s.img}>{children}</div>;
    if(src=='img2'){
       back = <div className={s.img2}>{children}</div>;
    }
    return (
        back
        );
}

export default BackgroundBlock;