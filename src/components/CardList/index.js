import React, { Component } from 'react';
import s from './CardList.module.scss';
import Card from "../Card";

class CardList extends Component{
    state={
        rus:'',
        eng:''
    }

    handlerInputSpellChange = (e) =>{
        console.log(e.target.value)
        this.setState({
                rus:e.target.value

        })
    }
    handlerInputValueChange = (e) =>{
        console.log(e.target.value)
        this.setState({
            eng:e.target.value

        })
    }
    handlerSubmitForm = (e) =>{
        e.preventDefault();
        this.props.onAddItem(this.state);

    }
    render() {
        const {item=[],onDeletedItem} = this.props;
        return (
            <>

                <form
                    className={s.form}
                    onSubmit={this.handlerSubmitForm}
                >

                    <input
                        type="text"
                        value={this.state.rus}
                        onChange={this.handlerInputSpellChange}
                    />
                    <input
                        type="text"
                        value={this.state.eng}
                        onChange={this.handlerInputValueChange}
                    />
                    <button>Добавить заклинание</button>
                </form>
                <div className={s.container}>
                    {item.map(({eng,rus,id},index) => (
                            <Card onDeleted={()=>{
                                console.log('level 2')
                                onDeletedItem(id)
                            }
                            } key={index} eng={eng} rus={rus}/>
                        )
                    )}
                </div>
            </>
        );
    }

}


export default CardList;
