import React from 'react';
import style from './ContentBlock.module.scss';

const ContentBlock = ({children}) => {
    return (
        <>
            {children}
        </>
        );
}

export default ContentBlock;
