export const wordsList = [
    {
        eng: 'Crucio',
        rus: 'Вызывает адскую боль',
        id:0,
    },
    {
        eng: 'Alohomora',
        rus: 'Отпирает замки, но не все',
        id:1,
    },
    {
        eng: 'Riddikulus.',
        rus: 'Превращает страшилище в посмешище',
        id:2,
    },
    {
        eng: 'Accio',
        rus: 'Притягивают к себе предмет',
        id:3,
    },
    {
        eng: 'Avada Kedavra',
        rus: 'Убивающее заклятие',
        id:4,
    },
    {
        eng: 'Imperio',
        rus: 'Управление противником',
        id:5,
    },
    {
        eng: 'Sonorus',
        rus: 'Увеличивает громкость голоса',
        id:6,
    },
    {
        eng: 'Reducto',
        rus: 'Взрывающее заклятье',
        id:7,
    },
    {
        eng: 'Expelliarmus',
        rus: 'Заклинание разоружения',
        id:8,
    },


];
