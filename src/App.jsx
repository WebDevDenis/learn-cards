import React,{Component} from 'react';
import HomePage from "./pages/Home";
import LoginPage from './pages/Login'
import RegistrationPage from "./pages/Registration";
import {Layout, Spin,Menu} from 'antd';
import s from './style.scss'
import FirebaseContext from "./context/firebaseContext";
import {BrowserRouter, Route, Link} from "react-router-dom";
import ContentBlock from "./components/ContentBlock";
import {PrivateRoute} from "./utils/privateRoute";
import {connect} from 'react-redux';

import {bindActionCreators} from "redux";
import {addUserAction} from "./actions/userAction";


class App extends Component {


    componentDidMount() {
        const {auth,setUserUid} = this.context;
        const {addUser} =this.props;
        auth.onAuthStateChanged(user => {
            if (user){
                setUserUid(user.uid);
                localStorage.setItem('user',JSON.stringify(user.uid));
                addUser(user);
            }else{
                setUserUid(null);
                localStorage.removeItem('user');
            }
        })
    }

    render() {
        const {userUid} = this.props;
        if (userUid===null){
            return (
                <div className={s.loader}>
                    <Spin size="large" />
                </div>
            );
        }
        return (
            <>
                <BrowserRouter>

                    <Route path="/login" component={LoginPage}/>
                    <Route path="/registration" component={RegistrationPage}/>
                    <Route render={(props)=>{

                        const {history:{push}} = props;
                        return ( <>

                            <Menu  theme="dark"  mode="horizontal">
                                <Menu.Item key='1'>
                                    <Link to="/">Главная</Link>
                                </Menu.Item>
                                <Menu.Item key='2'>
                                    <Link to="/login">Войти</Link>
                                </Menu.Item>
                                <Menu.Item key='3' onClick={()=>push('/contact')}>
                                    <Link to="/registration" >Регистрация</Link>
                                </Menu.Item>
                            </Menu>

                            <ContentBlock>
                                <PrivateRoute path="/" exact component={HomePage}/>
                                <PrivateRoute path="/home" component={HomePage}/>
                            </ContentBlock>
                        </>)
                    }
                    }/>
                </BrowserRouter>
                {/*{user?<HomePage user={user}/>:<LoginPage/>}*/}

            </>
        )
    }

}

App.contextType = FirebaseContext;

const mapStateToProps = (state) =>{
    return({
        userUid: state.user.userUid
    })
}
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        addUser:addUserAction
    })
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
