import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import FirebaseContext from "./context/firebaseContext";
import Firebase from "./services/firebase";
import  {createStore, bindActionCreators} from "redux";
import rootReducer from './reducers';
import {Provider} from 'react-redux';


const store = new createStore(rootReducer);
// const {dispatch} = store;
// const {plusAction,minusAction} = bindActionCreators(actions,dispatch)
//
// store.subscribe(()=>{
//     return console.log(store.getState());
// })
// plusAction(5);
// minusAction(1);


ReactDOM.render(
    <Provider store={store}>
    <FirebaseContext.Provider value={new Firebase()}>
        <App title="Заголовок через props" />
    </FirebaseContext.Provider>
    </Provider>,  document.getElementById('root'));

