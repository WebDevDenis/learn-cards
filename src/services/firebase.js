import * as firebase from 'firebase/app';
import 'firebase/database'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_apiKey,
    authDomain: process.env.REACT_APP_FIREBASE_authDomain,
    databaseURL: process.env.REACT_APP_FIREBASE_URL,
    projectId:  process.env.REACT_APP_FIREBASE_projectId,
    storageBucket: process.env.REACT_APP_FIREBASE_storageBucket,
    messagingSenderId: process.env.REACT_APP_FIREBASE_messagingSenderId,
    appId: process.env.REACT_APP_FIREBASE_appId
};

class Firebase{
    constructor() {
        firebase.initializeApp(firebaseConfig);

        this.auth = firebase.auth();
        this.database = firebase.database();
        this.userUid = null;
    }

    setUserUid = (uid) =>this.userUid = uid;

    signWithEmail = (email,password) => this.auth.signInWithEmailAndPassword(email,password);

    getUserCardRef = () => this.database.ref(`/cards/${this.userUid}`);

    logoutUser = () => this.auth.signOut().then(function() {
        console.log('####:','успешный выход')
    }).catch(function(error) {
        console.log('####: logout error',error)
    });

    newUser = (email,password) =>this.auth.createUserWithEmailAndPassword(email, password).catch(function(error) {
       console.log('####: error newUser', error);
    })

    getUserCurrentCardRef = (id) => this.database.ref(`/cards/${this.userUid}/${id}`);
}

export default Firebase;
